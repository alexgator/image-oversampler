import java.awt.image.BufferedImage
import java.io.File
import java.io.IOException
import javax.imageio.ImageIO

fun main(args: Array<String>) {
    val inputFile = File("lena.bmp")

    ImageOversampler(inputFile, 0.3, 0.3, File("lena_0.3-0.3.bmp"))
            .process()
    ImageOversampler(inputFile, 4.0, 4.0, File("lena_4.0_4.0.bmp"))
            .process()
    ImageOversampler(inputFile, 0.7, 1.5, File("lena_0.7_1.5.bmp"))
            .process()
    ImageOversampler(inputFile, 1.5, 0.7, File("lena_1.5_0.7.bmp"))
            .process()
}

class ImageOversampler(inFile: File,
                       private val widthMul: Double,
                       private val heightMul: Double,
                       private val outFile: File) {

    private val width: Int
    private val height: Int
    private val sampleHeight: Int
    private val sampleWidth: Int
    private val inImage: BufferedImage?
    private val outImage: BufferedImage

    init {
        try {
            inImage = ImageIO.read(inFile)
        } catch (e: IOException) {
            throw IOException("Error reading file \'$inFile\': ${e.message}", e)
        }

        inImage ?: throw IllegalStateException("No input image")

        width           = inImage.width
        height          = inImage.height
        sampleWidth     = (width * widthMul).toInt()
        sampleHeight    = (height * heightMul).toInt()
        outImage        = BufferedImage(sampleWidth, sampleHeight,BufferedImage.TYPE_INT_RGB)
    }

    fun process() {
        var bytes: Array<FloatArray>

        for (i in 0..2) {
            bytes = Array(height) { FloatArray(width) }

            for (y in 0 until height) {
                for (x in 0 until width) {
                    bytes[y][x] = (inImage!!.getRGB(x, y).ushr(i * 8) and 0xFF).toFloat()
                }
            }

            bytes = transpose(oversample(bytes, sampleWidth, widthMul))
            bytes = transpose(oversample(bytes, sampleHeight, heightMul))

            for (y in 0 until sampleHeight) {
                for (x in 0 until sampleWidth) {
                    var tmp = bytes[y][x]

                    if (tmp > 255) {
                        tmp = 255f
                    } else if (tmp < 0) {
                        tmp = 0f
                    }

                    outImage.setRGB(x, y, outImage.getRGB(x, y) or (Math.round(tmp) shl i * 8))
                }
            }
        }

        try {
            ImageIO.write(outImage, "bmp", outFile)
        } catch (e: IOException) {
            throw IOException("Error writing file \'$outFile\': ${e.message}", e)
        }
    }

    private fun oversample(image: Array<FloatArray>, newWidth: Int,
                           multiplier: Double): Array<FloatArray> {
        val oldWidth    = image[0].size
        val oldHeight   = image.size
        val sincScale   = Math.min(newWidth.toDouble() / oldWidth, 1.0)
        val result      = Array(oldHeight) { FloatArray(newWidth) }

        for (y in 0 until oldHeight) {
            for (x in 0 until newWidth) {

                var valueSum = 0.0
                var filterSum = 0.0
                val centerX = (x + 0.5) / newWidth * oldWidth
                val filterStartX = centerX - multiplier / 2

                val startIndex = Math.ceil(filterStartX - 0.5).toInt()

                for (i in 0..(Math.floor(multiplier).toInt())) {
                    val inputX  = startIndex + i
                    val weight  = sinc((inputX + 0.5 - centerX) * sincScale)
                    valueSum    += weight * image[y][Math.min(Math.max(inputX, 0), oldWidth - 1)]
                    filterSum   += weight
                }

                result[y][x] = (valueSum / filterSum).toFloat()
            }
        }
        return result
    }

    private fun sinc(x: Double): Double {
        var result  = x
        result      *= Math.PI
        return if (result != 0.0) Math.sin(result) / result else 1.0
    }

    private fun transpose(image: Array<FloatArray>): Array<FloatArray> {
        val result = Array(image[0].size) { FloatArray(image.size) }
        for (i in result.indices) {
            for (j in 0 until result[i].size)
                result[i][j] = image[j][i]
        }
        return result
    }
}

